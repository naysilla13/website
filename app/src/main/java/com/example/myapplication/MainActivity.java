package com.example.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView textViewResult;
    private String input = "";

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        textViewResult = findViewById(R.id.textViewResult);


        Button[] numberButtons = new Button[10];
        for (int i = 0; i < 10; i++) {
            String buttonID = "btn" + i;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            numberButtons[i] = findViewById(resID);
            numberButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onNumberClick(v);
                }
            });
        }

        Button btnPlus = findViewById(R.id.btnPlus);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOperatorClick(v);
            }
        });

        Button btnEqual = findViewById(R.id.btnEqual);
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEqualClick(v);
            }
        });
    }

    public void onNumberClick(View view) {
        Button button = (Button) view;
        input += button.getText().toString();
        editText.setText(input);
    }

    public void onOperatorClick(View view) {
        Button button = (Button) view;
        input += " " + button.getText().toString() + " ";
        editText.setText(input);
    }

    public void onEqualClick(View view) {
        try {
            String result = evaluateExpression(input);
            textViewResult.setText(result);
            input = result;
        } catch (Exception e) {
            textViewResult.setText("Error");
            input = "";
        }
    }

    private String evaluateExpression(String expression) {
        try {
            ScriptEngine engine = new javax.script.ScriptEngineManager().getEngineByName("rhino");
            Object result = engine.eval(expression);
            return result.toString();
        } catch (Exception e) {
            return "Error";
        }
    }

    private class ScriptEngine {
    }
}
